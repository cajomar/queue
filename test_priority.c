#include <stdio.h>
#include <assert.h>
#include "priority_queue.h"

#define LESS_THAN(a,b) ((a)<(b))
PRIORITY_QUEUE_TEMPLATE(pqueue, unsigned int, unsigned int, LESS_THAN)

#define TEST_COUNT (1000 * 100)
int a[TEST_COUNT];

int main(void) {
    assert(a);
    pqueue q;
    pqueue_init(&q, 10);
    for (long i=0; i<TEST_COUNT; i++) {
        a[i] = i;   
    }
    for (long i=TEST_COUNT-1; i>=0; i--) {
        unsigned long j = rand() % (i + 1);
        unsigned long tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
    printf("Shuffled\n");
    for (unsigned long i=0; i<TEST_COUNT; i++) {
        pqueue_push(&q, a[i], a[i]);
    }
    printf("Pushed\n");
    while (q.count) {
        pqueue_pop(&q);
    }
    pqueue_deinit(&q);
    return 0;
}
