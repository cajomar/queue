FLAGS='-Wall -Wextra -pedantic'
gcc test.c -ansi $FLAGS -g3 && valgrind ./a.out
gcc test_priority.c -std=c17 $FLAGS -g3 && valgrind ./a.out

gcc test.c -ansi $FLAGS -O2 && time ./a.out
gcc test_priority.c -std=c17 $FLAGS -O2 && time ./a.out
