README
======

My single-file header-only library for FIFO and binary-heap priority queues. See [queue.h](https://bitbucket.org/cajomar/queue/raw/HEAD/queue.h) and [priority_queue.h](https://bitbucket.org/cajomar/queue/raw/HEAD/priority_queue.h) for more.
