#include <stdio.h>
#include <assert.h>

#include "queue.h"

#define queue_log(q) printf("queue: %p, capacity: %lu, front: %lu, count: %lu, first: %d, last: %d\n", (void*)q, \
                        queue_raw(q)->capacity, \
                        queue_raw(q)->front, \
                        queue_raw(q)->count, \
                        queue_peek(q), \
                        queue_last(q))

#define CHECK_SIZE (1000 * 1000 * 10)

int main() {
    int* q = 0;
    int r;
    int i;

    assert(queue__normalize_index(-1, CHECK_SIZE) == CHECK_SIZE - 1);

    assert(printf("Doing assertions\n"));

    for (i=0; i<CHECK_SIZE; i++) {
        queue_push(q, i);
        assert(queue_last(q) == i);
    }
    for (i=0; i<CHECK_SIZE; i++) {
        r = queue_pop(q);
        assert(r == i);
    }

    assert(queue_empty(q));
    queue_clear(q);

    for (i=0; i<CHECK_SIZE*2; i++) {
        queue_push(q, i);
    }
    for (i=0; i<CHECK_SIZE; i++) {
        assert(queue_getv(q, i) == i);
        assert(queue_getv(q, -i-1) == CHECK_SIZE*2-i-1);
    }
    for (i=CHECK_SIZE*2-1; i>=0; i--) {
        r = queue_poplast(q);
        assert(r == i);
    }
    queue_clear(q);
    for (i=0; i<CHECK_SIZE/2; i++) {
        queue_push(q, i);
    }
    for (i=0; i<CHECK_SIZE; i++) {
        queue_push(q, i);
    }
    for (i=0; i<CHECK_SIZE/2; i++) {
        r = queue_pop(q);
        assert(r == i);
    }
    queue_reverse(q);
    for (i=0; i<CHECK_SIZE; i++) {
        r = queue_pop(q);
        assert(r == CHECK_SIZE-1-i);
    }
    queue_free(q);
    return 0;
}
